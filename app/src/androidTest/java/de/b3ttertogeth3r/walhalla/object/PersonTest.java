/*
 * Copyright (c) 2022-2023.
 *
 * Licensed under the Apace License, Version 2.0 (the "Licence"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 *  License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 *  either express or implied. See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package de.b3ttertogeth3r.walhalla.object;

import junit.framework.TestCase;

public class PersonTest extends TestCase {

    public void testIsEnabled() {
    }

    public void testIsPassword() {
    }

    public void testGetId() {
    }

    public void testGetPasswordString() {
    }

    public void testTestToString() {
    }

    public void testValidate() {
    }

    public void testGetFull_Name() {
    }

    public void testGetFirst_Name() {
    }

    public void testGetLast_Name() {
    }

    public void testGetSecurity() {
    }

    public void testGetValue() {
    }

    public void testSetValue() {
    }

    public void testGetViewAll() {
    }

    public void testGetNickname() {
    }

    public void testGetRank() {
    }

    public void testGetViewEdit() {
    }

    public void testGetMail() {
    }

    public void testGetMobile() {
    }

    public void testGetOrigin() {
    }

    public void testGetMajor() {
    }

    public void testGetJoined() {
    }

    public void testGetBirthday() {
    }

    public void testGetViewDisplay() {
    }

    public void testValidatePersonal() {
    }

    public void testValidateFratData() {
    }
}